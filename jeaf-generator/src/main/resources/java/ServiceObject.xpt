«EXTENSION java::Naming»
«EXTENSION java::ObjectMapper»
«EXTENSION java::GeneratorCommons»
«EXTENSION entity::ModelProperties»
«EXTENSION functions::Class»
«EXTENSION functions::NamedElement»
«EXTENSION functions::TemplateParameter»
«EXTENSION org::openarchitectureware::util::stdlib::io»

«IMPORT JMM»

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Generate service object 
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
«DEFINE ServiceObjectClass FOR ServiceObject»
  «IF generateServiceObjects() == true»
  «IF this.ownedOperation.size == 0 && this.hasCustomImplementation == false && this.hasDerivedProperties() == false && (this.compositeDataTypeCustomSerialization == false || (this.compositeDataTypeCustomSerialization == true && this.generateInternalViewOfCompositeDataType() == false))»
	«FILE packagePath() + "/" + name +".java" src_gen»
	«getFileHeader()»
	package «this.packageName()»;
	
	import com.anaptecs.jeaf.xfun.api.XFunMessages;
	import com.anaptecs.jeaf.xfun.api.XFun;
	
	«EXPAND functions::Javadoc::JavadocForType»
	«EXPAND java::Helper::GenerateGeneratedAnnotation»
	«EXPAND java::Helper::GenerateSuppressWarningsAnnotation»
	«EXPAND java::Validation::ValidationAnnotation»
	«EXPAND java::Helper::GenerateDeprecationAnnotation -»
	«EXPAND java::Jackson::JacksonClassAnnotations -»
	public «IF isAbstract»abstract«ENDIF» class «name»«this.getTemplateParams()» «EXPAND SuperClassDeclaration» «EXPAND InterfaceDeclarations» {
		/**
		 * Default serial version uid.
		 */
		private static final long serialVersionUID = 1L;
		
		«REM»Generate constants for all real attributes«ENDREM»
		«EXPAND java::Helper::GenerateConstantsForAttributeNames FOR ((uml::Class) this)»

		«IF needsToImplementIdentifiable() == true»
		«EXPAND java::Identifiable::GenerateIdentifiableDeclaration (this.getObjectIDClassName())»
		«ENDIF»
		
		«IF this.isCompositeDataType()»
			/**
			 * String representation of this object.
			 */
			private «IF this.isCompositeDataTypeReadonly()»final «ENDIF» String «this.getCompositeDateTypePublicFieldName()»;
		«ELSE»
			«EXPAND Attribute::PropertyDeclaration FOREACH ownedAttribute.select(e|e.isPropertySuppressed() == false)»
		«ENDIF»
	    
		«EXPAND java::Helper::GenerateCustomHeadersField»
					
		/**
		 * Default constructor is only intended to be used for deserialization by tools like Jackson for JSON. For "normal" object creation builder should be used instead.
		 */
		protected «name»( ) {
		  «IF needsToImplementIdentifiable() == true»
		  objectID = null;
		  «ENDIF»
		  «EXPAND ServiceObjectPlainConstructorInit»
		}

		/**
		 * Initialize object using the passed builder.
		 * 
		 * @param pBuilder Builder that should be used to initialize this object. The parameter must not be null.
		 */
		protected «name»(Builder«this.getTemplateParams()» pBuilder) {
			«IF superClass.isEmpty == false»        
			// Call constructor of super class.
			super(pBuilder);
			«ELSE»
			// Ensure that builder is not null.
			com.anaptecs.jeaf.xfun.api.checks.Check.checkInvalidParameterNull(pBuilder, "pBuilder");
			«ENDIF»
			«IF needsToImplementIdentifiable() == true»
			// Read object ID.
			com.anaptecs.jeaf.xfun.api.common.ObjectIdentity<?> lObjectID = pBuilder.objectID;
			if (lObjectID != null) {
				objectID = new «this.getObjectIDClassName()»(pBuilder.objectID);
		    }
		    else {
		      objectID = null;
		    }
			«ENDIF»
			«IF this.ownedAttribute.size > 0»
			// Read attribute values from builder.
			«ENDIF»
			«EXPAND ServiceObjectBuilderConstructorInit»
		}
		  
		«REM»Generate builder«ENDREM»
		«EXPAND java::Builder::StaticBuilderMethods FOR this»
		«EXPAND java::Helper::GenerateValueOfForOpenAPIDataType FOR this»
		«EXPAND java::Helper::GenerateOfForClass FOR this»
		«EXPAND java::Builder::GenerateBuilder (this.getObjectIDClassName())»
		
		«IF this.isCompositeDataType()»
			/**
			 * Method returns the String representation of this object.
			 * 
			 * @return String String representation of this object.
			 */
			public String get«this.getCompositeDateTypePublicFieldName().toFirstUpper()»( ) {
			  return «this.getCompositeDateTypePublicFieldName()»;
			}
			
			«IF this.isCompositeDataTypeReadonly() == false»
			/**
			 * Method returns the String representation of this object.
			 * 
			 * @return String String representation of this object.
			 */
			public void set«this.getCompositeDateTypePublicFieldName().toFirstUpper()»( String p«this.getCompositeDateTypePublicFieldName().toFirstUpper()» ) {
			  «this.getCompositeDateTypePublicFieldName()» = p«this.getCompositeDateTypePublicFieldName().toFirstUpper()»;
			}
			«ENDIF»
		«ELSE»
	        «IF needsToImplementIdentifiable() == true»
				«EXPAND java::Identifiable::GenerateIdentifiableGetters (this.getObjectIDClassName())»
	        «ENDIF»
	
			«EXPAND Attribute::PropertyAccessors FOREACH ownedAttribute.select(e|e.isPropertySuppressed() == false)»
		«ENDIF»
		«EXPAND java::Helper::GenerateCustomHeadersOperations»
		«EXPAND java::Helper::GenerateEqualsHashCode(name)»
		«EXPAND java::Helper::GenerateToStringMethod»
		«EXPAND java::Builder::ToBuilderMethod»
	}
	«ENDFILE»
	«REM»ServiceObject has own custom implementation«ENDREM»
	«ELSE»
      «FILE packagePath() + "/" + name +"Base.java" src_gen»
      «getFileHeader()»
      package «this.packageName()»;
      
      import com.anaptecs.jeaf.xfun.api.XFunMessages;
      import com.anaptecs.jeaf.xfun.api.XFun;        
      
      «EXPAND functions::Javadoc::JavadocForType»
	  «EXPAND java::Helper::GenerateGeneratedAnnotation»
	  «EXPAND java::Helper::GenerateSuppressWarningsAnnotation»
      «EXPAND java::Validation::ValidationAnnotation -»      
      «EXPAND java::Helper::GenerateDeprecationAnnotation-»
      public abstract class «name»Base«this.getTemplateParams()» «EXPAND SuperClassDeclaration» «EXPAND InterfaceDeclarations» {
        /**
         * Default serial version uid.
         */
        private static final long serialVersionUID = 1L;
        
		«REM»Generate constants for all real attributes«ENDREM»
		«EXPAND java::Helper::GenerateConstantsForAttributeNames FOR ((uml::Class) this)»
        
		«IF needsToImplementIdentifiable() == true»
		«EXPAND java::Identifiable::GenerateIdentifiableDeclaration (this.getObjectIDClassName())»
		«ENDIF»
		
		«IF this.isCompositeDataType()»
			/**
			 * String representation of this object.
			 */
			private «IF this.isCompositeDataTypeReadonly()»final «ENDIF» String «this.getCompositeDateTypePublicFieldName()»;
		«ELSE»
			«EXPAND Attribute::PropertyDeclaration FOREACH ownedAttribute.select(e|e.isPropertySuppressed() == false)»
		«ENDIF»

		«EXPAND java::Helper::GenerateCustomHeadersField»
					
		/**
		 * Default constructor is only intended to be used for deserialization by tools like Jackson for JSON. For "normal" object creation builder should be used instead.
		 */
		protected «name»Base( ) {
		  «IF needsToImplementIdentifiable() == true»
		  objectID = null;
		  «ENDIF»
		  «EXPAND ServiceObjectPlainConstructorInit»
		}

		/**
		 * Initialize object using the passed builder.
		 * 
		 * @param pBuilder Builder that should be used to initialize this object. The parameter must not be null.
		 */
		protected «name»Base(BuilderBase«this.getTemplateParams()» pBuilder) {
		«IF superClass.isEmpty == false»        
		// Call constructor of super class.
		super(pBuilder);
		«ELSE»
		// Ensure that builder is not null.
		com.anaptecs.jeaf.xfun.api.checks.Check.checkInvalidParameterNull(pBuilder, "pBuilder");
		«ENDIF»
		«IF needsToImplementIdentifiable() == true»
		// Read object ID.
		com.anaptecs.jeaf.xfun.api.common.ObjectIdentity<?> lObjectID = pBuilder.objectID;
		if (lObjectID != null) {
			objectID = new «this.getObjectIDClassName()»(pBuilder.objectID);
	    }
	    else {
	      objectID = null;
	    }
		«ENDIF»
		«IF this.ownedAttribute.size > 0»
		// Read attribute values from builder.
		«ENDIF»
		«EXPAND ServiceObjectBuilderConstructorInit»
		}
		
		«REM»Generate builder«ENDREM»
		«EXPAND java::Builder::GenerateBuilderBase (this.getObjectIDClassName())»
					
		«IF this.isCompositeDataType()»
			/**
			 * Method returns the String representation of this object.
			 * 
			 * @return String String representation of this object.
			 */
			public String get«this.getCompositeDateTypePublicFieldName().toFirstUpper()»( ) {
			  return «this.getCompositeDateTypePublicFieldName()»;
			}
			
			«IF this.isCompositeDataTypeReadonly() == false»
			/**
			 * Method returns the String representation of this object.
			 * 
			 * @return String String representation of this object.
			 */
			public void set«this.getCompositeDateTypePublicFieldName().toFirstUpper()»( String p«this.getCompositeDateTypePublicFieldName().toFirstUpper()» ) {
			  «this.getCompositeDateTypePublicFieldName()» = p«this.getCompositeDateTypePublicFieldName().toFirstUpper()»;
			}
			«ENDIF»
		«ELSE»
	        «IF needsToImplementIdentifiable() == true»
				«EXPAND java::Identifiable::GenerateIdentifiableGetters (this.getObjectIDClassName())»
	        «ENDIF»
	
			«EXPAND Attribute::PropertyAccessors FOREACH ownedAttribute.select(e|e.isPropertySuppressed() == false)»
		«ENDIF»

        «EXPAND java::Helper::GenerateOfForClass FOR this»
        «EXPAND java::Attribute::AbstractGetterForDerivedProperty FOREACH this.getDerivedProperties()»
        «EXPAND jeaf::JEAFOperation::PersistentObjectOperation FOREACH ownedOperation»
        «EXPAND java::Helper::GenerateCustomHeadersOperations»
        «EXPAND java::Helper::GenerateEqualsHashCode(name + "Base")»
        «EXPAND java::Helper::GenerateToStringMethod»
        «EXPAND java::Builder::ToBuilderMethodForBaseClass»
      }
      «ENDFILE»

	  «FILE packagePath() + "/" + name +".java" src»
	  «getFileHeader()»
	  package «this.packageName()»;
	  
	  
	  «EXPAND functions::Javadoc::JavadocForType»
	  «EXPAND java::Validation::ValidationAnnotation»
	  «EXPAND java::Helper::GenerateDeprecationAnnotation -»
	  «EXPAND java::Jackson::JacksonClassAnnotations -»
	  public «IF isAbstract»abstract«ENDIF» class «name»«this.getTemplateParams()» extends «name»Base«this.getTemplateParams()» {
	    /**
	     * Default serial version uid.
	     */
	    private static final long serialVersionUID = 1L;
	    
		/**
		 * Default constructor is only intended to be used for deserialization by tools like Jackson for JSON. For "normal" object creation builder should be used instead.
		 */
		protected «name»( ) {
		}
		
		/**
		 * Initialize object. Nothing special to do.
		 */
		protected «name»( «name».BuilderBase«this.getTemplateParams()» pBuilder ) {
		  super(pBuilder);
		}
		  
		«EXPAND java::Builder::StaticBuilderMethods FOR this»
		«EXPAND java::Helper::GenerateValueOfForOpenAPIDataType FOR this»
		«EXPAND java::Builder::GenerateBuilderImpl»

		«EXPAND java::Attribute::EmptyGetterImplForDerivedProperty FOREACH this.getDerivedProperties()»
		«EXPAND jeaf::JEAFOperation::PersistentObjectOperationImpl FOREACH ownedOperation»

		  «IF this.compositeDataTypeCustomSerialization»
		  /**
		   * Method serializes this object into its custom string representation.
		   * 
		   * @return {@link String} Serialized String representation of this object. The method must not return null.
		   */
		  public String serialize( ) {
		    // TODO Implementation required
		    return null;
		  }
		
		  /**
		   * Method deserialized the passed String representation of this object into a new instance of this class.
		   * 
		   * @param pSerialized Serialized representation that should be used to create a new instance. The parameter is never null.
		   * @return {@link «name»} Instance of this class as it was deserialized from the passed string. The method must not return null.
		   */
		  public static «name» deserialize( String pSerialized ) {
		    // TODO Implementation required
		    return null;
		  }
		  «ENDIF»
	  }
	  «ENDFILE»
	«ENDIF»
	«ENDIF»
		
	«REM»Generate Object Mappers for POJO«ENDREM»
	«EXPAND java::ObjectMapper::GenerateObjectMappers»
«ENDDEFINE»

«DEFINE SuperClassDeclaration FOR ServiceObject»
	«IF !superClass.isEmpty» extends «superClass.get(0).fqn()»«ENDIF»
«ENDDEFINE»

«DEFINE InterfaceDeclarations FOR ServiceObject»
	«IF superClass.isEmpty»
		implements com.anaptecs.jeaf.core.api.ServiceObject
		«IF needsToImplementIdentifiable()==true», com.anaptecs.jeaf.xfun.api.common.Identifiable<«this.getObjectIDClassName()»>«ENDIF»
	«ELSE»
		«IF needsToImplementIdentifiable()==true»implements com.anaptecs.jeaf.xfun.api.common.Identifiable<«this.getObjectIDClassName()»>«ENDIF»
	«ENDIF»
«ENDDEFINE»


«DEFINE ServiceObjectPlainConstructorInit FOR ServiceObject»
	«IF this.isCompositeDataType()»
		«IF this.isCompositeDataTypeReadonly()»
			«this.getCompositeDateTypePublicFieldName()» = null;
		«ENDIF»
	«ELSE»
		«FOREACH this.ownedAttribute.select(e|e.isPropertySuppressed() == false) AS attr»
			«IF attr.isReadOnly() && attr.isStatic == false»
				«IF attr.type.isPrimitiveType() == true»
					«IF attr.isMultivalued() == false»
						«IF attr.isRealInitValue()»
							«attr.name» = «attr.initValue()»;
						«ELSE»
							«attr.name» = «attr.getPrimitiveDefaultValue()»;
						«ENDIF»
					«ELSE»
						«attr.name» = null;
					«ENDIF»
				«ELSE»
					«IF attr.isMultivalued()»
						«REM»Distiguish between arrays and collections.«ENDREM»
						«IF attr.association == null»
							«attr.name» = null;
						«ELSE»
							«IF attr.isSoftLink()»
							    «attr.name» = new «attr.getCollectionImplType()»<«((SoftLink)attr).getMultivaluedJavaSoftLinkType()»>();
							«ELSE»
							    «attr.name» = new «attr.getCollectionImplType()»<«attr.type.fqn()»>();
							«ENDIF»
						«ENDIF»
					«ELSE»
						«attr.name» = null;
					«ENDIF»
				«ENDIF»
			«ELSE»
				«IF attr.isMultivalued() && attr.association != null»
					«REM»Only in case of collections we have to initilize them.«ENDREM»
					«IF attr.isSoftLink()»
					   «attr.name» = new «attr.getCollectionImplType()»<«((SoftLink)attr).getMultivaluedJavaSoftLinkType()»>();
					«ELSE»
					   «attr.name» = new «attr.getCollectionImplType()»<«attr.type.fqn()»>();
					«ENDIF»
				«ELSE»
					«IF attr.isRealInitValue() && attr.isStatic == false »
						«attr.name» = «IF attr.isJEAFEnumeration()»«attr.type.name».«ENDIF»«attr.initValue()»;
					«ENDIF»
				«ENDIF»
			«ENDIF»
			«IF attr.isTransientBidirectionalBackReference()»
				// Bidirectional back reference is not yet set up correctly
				«attr.asInstanceVar()»BackReferenceInitialized = false;
			«ENDIF»
		«ENDFOREACH»
	«ENDIF»
«ENDDEFINE»

«DEFINE ServiceObjectBuilderConstructorInit FOR ServiceObject»
	«IF this.isCompositeDataType()»
		«this.getCompositeDateTypePublicFieldName()» = pBuilder.«this.getCompositeDateTypePublicFieldName()»;
	«ELSE»
		«FOREACH this.ownedAttribute.select(e|e.isPropertySuppressed() == false) AS attr»
			«IF attr.association == null»
				«IF attr.isStatic == false»
					«attr.name» = pBuilder.«attr.name»;
				«ENDIF»
			«ELSE»
				«IF attr.isMultivalued()»
					«IF attr.isStatic == false»
						if(pBuilder.«attr.name» != null) {
						«attr.name» = pBuilder.«attr.name»;
						}
						else {
						«IF attr.isSoftLink()»
						  «attr.name» = new «attr.getCollectionImplType()»<«((SoftLink)attr).getMultivaluedJavaSoftLinkType()»>();
						«ELSE»
						  «attr.name» = new «attr.getCollectionImplType()»<«attr.type.fqn()»>();
						«ENDIF»
						}
					«ENDIF»
				«ELSE»
					«IF attr.isStatic == false»
						«attr.name» = pBuilder.«attr.name»;
					«ENDIF»			  	
				«ENDIF»
			«ENDIF»			  
			«IF attr.isTransientBidirectionalBackReference()»
				// Bidirectional back reference is set up correctly as a builder is used.
				«attr.asInstanceVar()»BackReferenceInitialized = true;
			«ENDIF»
		«ENDFOREACH»
	«ENDIF»
«ENDDEFINE»