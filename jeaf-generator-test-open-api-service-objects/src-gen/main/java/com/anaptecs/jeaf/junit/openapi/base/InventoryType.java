package com.anaptecs.jeaf.junit.openapi.base;

public enum InventoryType {
  SBB, SNCF, DB;
}