# Hello Reports

<br>


| Type     | Legacy Name | Package | Description  |
|----------|---------------------|-----------|------------|
AbstractPOJO |   | com.anaptecs.jeaf.junit.pojo |  |
AbstractResponse |   | com.anaptecs.jeaf.junit.rest.generics |  |
Account |   | com.anaptecs.jeaf.accounting.impl.pojo |  |
Account |   | com.anaptecs.jeaf.accounting |  |
AccountInfo |   | com.anaptecs.jeaf.accounting |  |
AdvancedPOJO |   | com.anaptecs.jeaf.junit.pojo |  |
AndOneMorePOJO |   | com.anaptecs.jeaf.junit.generics |  |
Assortment |   | com.anaptecs.jeaf.junit.product.pojo |  |
BChildPOJO |   | com.anaptecs.jeaf.junit.pojo |  |
BParentPOJO |   | com.anaptecs.jeaf.junit.pojo |  |
Bank |   | com.anaptecs.jeaf.accounting |  |
BankAccount |   | com.anaptecs.jeaf.junit.openapi.base |  |
BankType |   | com.anaptecs.jeaf.accounting |  |
BeanParamWithDeprecations |   | com.anaptecs.jeaf.junit.deprecation |  |
BeanParameter |   | com.anaptecs.jeaf.junit.openapi.base |  |
BidirectA |   | com.anaptecs.jeaf.junit.openapi.base |  |
BidirectB |   | com.anaptecs.jeaf.junit.openapi.base |  |
BidirectionalA |   | com.anaptecs.jeaf.junit.pojo |  |
BidirectionalB |   | com.anaptecs.jeaf.junit.pojo |  |
BigDecimalCode |   | com.anaptecs.jeaf.junit.openapi.base |  |
BigIntegerCode |   | com.anaptecs.jeaf.junit.openapi.base |  |
Booking |   | com.anaptecs.jeaf.junit.openapi.base |  |
Booking |   | com.anaptecs.jeaf.accounting.impl.pojo |  |
Booking |   | com.anaptecs.jeaf.accounting |  |
BookingCode |   | com.anaptecs.jeaf.junit.openapi.base |  |
BookingID |   | com.anaptecs.jeaf.junit.openapi.base | Type represents a booking ID. |
BooleanCode |   | com.anaptecs.jeaf.junit.openapi.base |  |
BooleanCodeType |   | com.anaptecs.jeaf.junit.openapi.base |  |
BooleanLiteralsEnum |   | com.anaptecs.jeaf.junit.openapi.base |  |
BusinessA |   | com.anaptecs.jeaf.junit.openapi.techbase |  |
BusinessChild |   | com.anaptecs.jeaf.junit.openapi.techbase |  |
BusinessParent |   | com.anaptecs.jeaf.junit.openapi.techbase |  |
BusinessServiceObject |   | com.anaptecs.jeaf.junit.rest.generics |  |
ByteCode |   | com.anaptecs.jeaf.junit.openapi.base |  |
ByteCodeType |   | com.anaptecs.jeaf.junit.openapi.base |  |
CHStopPlace |   | com.anaptecs.jeaf.junit.openapi.base |  |
Campaign |   | com.anaptecs.jeaf.junit.openapi.base |  |
Channel |   | com.anaptecs.jeaf.junit.openapi.base |  |
ChannelCode |   | com.anaptecs.jeaf.junit.openapi.base |  |
ChannelType |   | com.anaptecs.jeaf.junit.openapi.base |  |
ChildA |   | com.anaptecs.jeaf.junit.openapi.base | single line class comment |
ChildAA |   | com.anaptecs.jeaf.junit.openapi.base |  |
ChildB |   | com.anaptecs.jeaf.junit.openapi.base | Multi<br>line<br>class<br>comment |
ChildBB |   | com.anaptecs.jeaf.junit.openapi.base |  |
ChildBeanParameterType |   | com.anaptecs.jeaf.junit.openapi.service1 |  |
ChildPOJO |   | com.anaptecs.jeaf.junit.pojo | This is the first line of the first comment<br>2nd line |
ClassA |   | com.anaptecs.jeaf.junit.core |  |
ClassB |   | com.anaptecs.jeaf.junit.core |  |
CodeTypeUsageTest |   | com.anaptecs.jeaf.junit.openapi.base |  |
Color |   | com.anaptecs.jeaf.junit.core | There are so many colors. |
Company |   | com.anaptecs.jeaf.junit.openapi.base |  |
Company |   | com.anaptecs.jeaf.accounting |  |
ComplexArrayServiceObject |   | com.anaptecs.jeaf.junit.core |  |
ComplexBookingID |   | com.anaptecs.jeaf.junit.openapi.base |  |
ComplexBookingType |   | com.anaptecs.jeaf.junit.openapi.base |  |
ComplextTypeArrayPOJO |   | com.anaptecs.jeaf.junit.pojo |  |
CompositeID |   | com.anaptecs.jeaf.junit.openapi.base |  |
Context |   | com.anaptecs.jeaf.junit.openapi.base |  |
CurrencyCode |   | com.anaptecs.jeaf.junit.openapi.base |  |
CustomEnum |   | com.anaptecs.jeaf.accounting.validation |  |
CustomPrimitiveArraysObjectWithRestrictions |   | com.anaptecs.jeaf.junit.pojo |  |
Customer |   | com.anaptecs.jeaf.accounting |  |
Customer |   | com.anaptecs.jeaf.accounting.impl.pojo |  |
DataTypeWithConstraints |   | com.anaptecs.jeaf.junit.openapi.base |  |
DataUnit |   | com.anaptecs.jeaf.junit.openapi.base |  |
DateQueryParamsBean |   | com.anaptecs.jeaf.junit.openapi.service1 |  |
DeprecatedContext |   | com.anaptecs.jeaf.junit.openapi.base |  |
DeprecatedServiceObject |   | com.anaptecs.jeaf.junit.core |  |
DirectedEdge |   | com.anaptecs.jeaf.junit.openapi.base | Just a simple comment. |
DiscountOffer |   | com.anaptecs.jeaf.junit.openapi.base |  |
DoubleCode |   | com.anaptecs.jeaf.junit.openapi.base |  |
DoubleCodeType |   | com.anaptecs.jeaf.junit.openapi.base |  |
Entity |   | com.anaptecs.jeaf.junit.openapi.base |  |
EnumTest |   | com.anaptecs.jeaf.junit.openapi.base |  |
EnumWithProperties |   | com.anaptecs.jeaf.junit.pojo |  |
ExtensibleEnum |   | com.anaptecs.jeaf.junit.openapi.base |  |
ExtensibleEnumWithProperties |   | com.anaptecs.jeaf.junit.pojo |  |
Farbe |   | com.anaptecs.jeaf.junit.core |  |
FloatCode |   | com.anaptecs.jeaf.junit.openapi.base |  |
FloatCodeType |   | com.anaptecs.jeaf.junit.openapi.base |  |
GenericPageableResponse |   | com.anaptecs.jeaf.junit.rest.generics |  |
GenericResponsePOJO |   | com.anaptecs.jeaf.junit.generics |  |
GenericSingleValuedReponse |   | com.anaptecs.jeaf.junit.rest.generics |  |
GeoPosition |   | com.anaptecs.jeaf.junit.openapi.base |  |
HeavyDataTypeUser |   | com.anaptecs.jeaf.junit.openapi.base |  |
IBAN |   | com.anaptecs.jeaf.junit.openapi.base |  |
IdentifiableServiceObject |   | com.anaptecs.jeaf.junit.core |  |
IdentifiableServiceObjectWithMethod |   | com.anaptecs.jeaf.junit.core |  |
IgnoringClass |   | com.anaptecs.jeaf.junit.openapi.base |  |
ImmutableAssociationPOJO |   | com.anaptecs.jeaf.junit.pojo |  |
ImmutableChildPOJO |   | com.anaptecs.jeaf.junit.pojo |  |
ImmutablePOJO |   | com.anaptecs.jeaf.junit.pojo |  |
ImmutablePOJOParent |   | com.anaptecs.jeaf.junit.pojo |  |
Individual |   | com.anaptecs.jeaf.accounting |  |
Input |   | com.anaptecs.jeaf.junit.otherpackage |  |
InputSubclass |   | com.anaptecs.jeaf.junit.otherpackage |  |
IntegerCode |   | com.anaptecs.jeaf.junit.openapi.base |  |
IntegerCodeType |   | com.anaptecs.jeaf.junit.openapi.base |  |
InventoryType |   | com.anaptecs.jeaf.junit.openapi.base |  |
JustAType |   | com.anaptecs.jeaf.junit.deprecation |  |
Leg |   | com.anaptecs.jeaf.junit.openapi.base |  |
LinkObject |   | com.anaptecs.jeaf.junit.openapi.base |  |
LocalBeanParamType |   | com.anaptecs.jeaf.junit.openapi.service1 |  |
LongCode |   | com.anaptecs.jeaf.junit.openapi.base |  |
LongCodeType |   | com.anaptecs.jeaf.junit.openapi.base |  |
MappingChild |   | com.anaptecs.jeaf.junit.objectmapping |  |
MappingParent |   | com.anaptecs.jeaf.junit.objectmapping |  |
Message |   | com.anaptecs.jeaf.junit.rest.generics |  |
Message |   | com.anaptecs.jeaf.junit.generics |  |
ModelWrapperArrayServiceObject |   | com.anaptecs.jeaf.junit.core |  |
ModelWrapperTypeServiceObject |   | com.anaptecs.jeaf.junit.core |  |
MultiValuedDataType |   | com.anaptecs.jeaf.junit.openapi.base |  |
MultivaluedQueryParamsBean |   | com.anaptecs.jeaf.junit.openapi.base |  |
MutableChildPOJO |   | com.anaptecs.jeaf.junit.pojo |  |
MyBusinessObject |   | com.anaptecs.jeaf.junit.generics |  |
NotInlinedBeanParam |   | com.anaptecs.jeaf.junit.openapi.base |  |
Offer |   | com.anaptecs.jeaf.junit.rest.generics |  |
OtherMappingObject |   | com.anaptecs.jeaf.junit.objectmapping |  |
Output |   | com.anaptecs.jeaf.junit.otherpackage |  |
POI |   | com.anaptecs.jeaf.junit.openapi.base |  |
POJOWithID |   | com.anaptecs.jeaf.junit.pojo |  |
POJOWithIDnMethod |   | com.anaptecs.jeaf.junit.pojo |  |
Pageable |   | com.anaptecs.jeaf.junit.rest.generics |  |
ParentBeanParamType |   | com.anaptecs.jeaf.junit.openapi.base |  |
ParentClass |   | com.anaptecs.jeaf.junit.openapi.base |  |
ParentPOJO |   | com.anaptecs.jeaf.junit.pojo |  |
PartiallyDeprecatedServiceObject |   | com.anaptecs.jeaf.junit.core |  |
Partner |   | com.anaptecs.jeaf.junit.openapi.base |  |
Partner |   | com.anaptecs.jeaf.accounting.impl.pojo |  |
PartnerContainer |   | com.anaptecs.jeaf.junit.openapi.base |  |
Person |   | com.anaptecs.jeaf.junit.openapi.base |  |
Person |   | com.anaptecs.jeaf.accounting |  |
PlaceRef |   | com.anaptecs.jeaf.junit.openapi.base |  |
PlainPOJO |   | com.anaptecs.jeaf.junit.pojo |  |
PostalAddress |   | com.anaptecs.jeaf.junit.openapi.base |  |
Price |   | com.anaptecs.jeaf.junit.product.pojo |  |
PrimitiveArrayServiceObject |   | com.anaptecs.jeaf.junit.core |  |
PrimitiveArraysObjectWithRestrictions |   | com.anaptecs.jeaf.junit.pojo |  |
PrimitiveArraysObjectWithRestrictions |   | com.anaptecs.jeaf.junit.openapi.base |  |
PrimitiveObjectWithRestrictions |   | com.anaptecs.jeaf.junit.pojo |  |
PrimitiveObjectWithRestrictions |   | com.anaptecs.jeaf.junit.openapi.base |  |
PrimitiveReferenceServiceObject |   | com.anaptecs.jeaf.junit.objectmapping |  |
PrimitiveServiceObject |   | com.anaptecs.jeaf.junit.core |  |
PrincipalWrapper |   | com.anaptecs.jeaf.junit.core |  |
Product |   | com.anaptecs.jeaf.junit.product.pojo |  |
Product |   | com.anaptecs.jeaf.junit.openapi.base | Data type represents a product definition |
ProductCategory |   | com.anaptecs.jeaf.junit.product |  |
ProductCode |   | com.anaptecs.jeaf.junit.openapi.base |  |
ReadonlyDefaultPOJO |   | com.anaptecs.jeaf.junit.pojo |  |
ReadonlyServiceObject |   | com.anaptecs.jeaf.junit.core |  |
Reseller |   | com.anaptecs.jeaf.junit.openapi.base |  |
Response |   | com.anaptecs.jeaf.junit.rest.generics |  |
ResponsibilityType |   | com.anaptecs.jeaf.accounting |  |
Sale |   | com.anaptecs.jeaf.junit.openapi.service1 | Chännel<br>€<br>Ö |
SchufaRequest |   | com.anaptecs.jeaf.accounting.impl |  |
SecurityToken |   | com.anaptecs.jeaf.accounting |  |
SessionContextValues |   | com.anaptecs.jeaf.junit.core |  |
ShortCode |   | com.anaptecs.jeaf.junit.openapi.base |  |
ShortCodeType |   | com.anaptecs.jeaf.junit.openapi.base |  |
SimpleDatatypeServiceObject |   | com.anaptecs.jeaf.junit.core |  |
SoftLinkChildA |   | com.anaptecs.jeaf.junit.pojo.softlink |  |
SoftLinkChildB |   | com.anaptecs.jeaf.junit.pojo.softlink |  |
SoftLinkParent |   | com.anaptecs.jeaf.junit.pojo.softlink |  |
SoftLinkPartner |   | com.anaptecs.jeaf.junit.pojo.softlink |  |
Sortiment |   | com.anaptecs.jeaf.junit.openapi.base |  |
SpecialContext |   | com.anaptecs.jeaf.junit.openapi.base |  |
Stop |   | com.anaptecs.jeaf.junit.openapi.base |  |
StopPlaceRef |   | com.anaptecs.jeaf.junit.openapi.base |  |
StringCode |   | com.anaptecs.jeaf.junit.openapi.base |  |
StringCodeType |   | com.anaptecs.jeaf.junit.openapi.base |  |
SubclassWithID |   | com.anaptecs.jeaf.junit.core |  |
SubclassWithIDnMethod |   | com.anaptecs.jeaf.junit.core |  |
SubclassWithoutID |   | com.anaptecs.jeaf.junit.core |  |
SubclassWithoutIDnMethod |   | com.anaptecs.jeaf.junit.core |  |
SwiftAccount |   | com.anaptecs.jeaf.accounting |  |
SwissGeoPosition |   | com.anaptecs.jeaf.junit.openapi.base |  |
TechParent |   | com.anaptecs.jeaf.junit.openapi.techbase |  |
TechnicalHeaderContext |   | com.anaptecs.jeaf.junit.openapi.service1 |  |
TestServiceObject |   | com.anaptecs.jeaf.junit.core | This is a service object that was modeled in order to test the output of the JEAF Generator. |
TheReadOnlyServiceObject |   | com.anaptecs.jeaf.junit.openapi.base |  |
TimeUnit |  Zeiteinheit | com.anaptecs.jeaf.junit.openapi.base | Enumeration represents time units.<br>     <br>NOVA 14 Mapping<br>* nova-base.xsd.ZeitEinheit |
TopoRef |   | com.anaptecs.jeaf.junit.openapi.base |  |
UICStop |  Haltestelle | com.anaptecs.jeaf.junit.openapi.base |  |
UICStopPlace |   | com.anaptecs.jeaf.junit.openapi.base |  |
VAT |   | com.anaptecs.jeaf.junit.product.pojo |  |
ValidationTestObject |   | com.anaptecs.jeaf.junit.core |  |
VerkehrsmittelTyp |   | com.anaptecs.jeaf.junit.domainObjects |  |
VersionedObjectSoftLink |   | com.anaptecs.jeaf.junit.openapi.base |  |
WeirdBooking |   | com.anaptecs.jeaf.junit.openapi.base |  |
WeirdParent |   | com.anaptecs.jeaf.junit.openapi.base |  |
WrapperArrayServiceObject |   | com.anaptecs.jeaf.junit.core |  |
WrapperDatatypeServiceObject |   | com.anaptecs.jeaf.junit.core |  |
WrapperTypesServiceObject |   | com.anaptecs.jeaf.junit.core |  |
YetAnotherPOJO |   | com.anaptecs.jeaf.junit.generics |  |


<br>

