![Latest Version](https://maven-badges.herokuapp.com/maven-central/com.anaptecs.jeaf.generator/jeaf-generator-project/badge.svg)


# JEAF Generator README #

This repository contains the source code for JEAF Generator. JEAF Generator can be used for code generation based on UML models. Besides the code of the generator itself the repository also contains lot's of test projects that are used to ensure quality. 


## Links ##
For further information please refer to our documentation:

* [JEAF Generator](https://anaptecs.atlassian.net/l/cp/roLu4d09)
* [JEAF Modelling Guide](https://anaptecs.atlassian.net/l/c/1B2ci31g)

## How do I get set up? ##

* Create a clone of this repository on your local machine.
* You will find a top level Maven project and lot's of sub projects. Each of them shows the specific output for one topic as it will be created by JEAF Generator 
* To build the project simply execute Maven on the top level project `maven clean install`
